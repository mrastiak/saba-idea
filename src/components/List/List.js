import React from "react";
import ListItem from "components/ListItem/ListItem";
import styles from "./List.module.css";

function List({ items }) {
  return (
    <div className={styles.list}>
      {items.map((item) => (
        <ListItem data={item} key={item.id} />
      ))}
    </div>
  );
}

export default List;
