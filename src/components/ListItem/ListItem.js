import React, { useRef, useEffect, useState } from "react";
import { useSelector } from "react-redux";
import urlify from "helpers/urlify";
import intersectionObserver from "utils/intersectionObserver";
import styles from "./ListItem.module.css";

function ListItem({ data }) {
  const currentVideoId = useSelector((state) => state);
  const [muted, setMuted] = useState(true);
  const ref = useRef(null);

  function handleClick(e) {
    e.preventDefault();
    setMuted(!muted);
  }

  useEffect(() => {
    if (ref.current) {
      intersectionObserver.observe(ref.current);
    }
  }, []);

  useEffect(() => {
    if (currentVideoId === data.id) {
      ref.current.play();
    } else if (!ref.current.paused) {
      ref.current.load();
    }
  }, [currentVideoId, data.id]);
  return (
    <div className={styles.listItem}>
      <video
        src={data.attributes.preview_src}
        poster={data.attributes.big_poster}
        className={styles.video}
        muted={muted}
        id={data.id}
        loop
        ref={ref}
        onClick={handleClick}
      />
      <div className={styles.dataCount}>
        <span role="img" aria-label="comments">
          &#128172;
        </span>
        <p className={styles.commentCount}>{data.attributes.comment_cnt}</p>
        <span role="img" aria-label="likes">
          &#10084;&#65039;
        </span>
        <p className={styles.likeCount}>{data.attributes.like_cnt}</p>
      </div>
      <p
        className={styles.description}
        dangerouslySetInnerHTML={{
          __html: urlify(data.attributes.description),
        }}
      ></p>
    </div>
  );
}

export default ListItem;
