import React from "react";
import { ReactComponent as Logo } from "assets/sabaidea-logo.svg";
import styles from "./Header.module.css";

function Header() {
  return (
    <header className={styles.header}>
      <Logo className={styles.logo} />
    </header>
  );
}

export default Header;
