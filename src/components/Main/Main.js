import React, { useState, useCallback, useEffect } from "react";
import List from "components/List/List";
import axios from "utils/axios";
import styles from "./Main.module.css";

function Main() {
  const [videos, setVideos] = useState([]);

  const loadVideos = useCallback(async () => {
    try {
      const response = await axios.get(
        "http://api.aparat.com/fa/v1/video/video/mostViewedVideos"
      );
      setVideos(response.data.data);
    } catch (e) {}
  }, []);

  useEffect(() => {
    loadVideos();
  }, [loadVideos]);

  return (
    <main className={styles.main} id="main">
      <List items={videos} />
    </main>
  );
}

export default Main;
