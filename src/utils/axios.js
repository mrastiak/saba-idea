import axios from "axios";

const instance = axios.create({
  baseURL: "http://api.aparat.com/fa/v1/",
});

export default instance;
