let selectedVideo = null;

function filterTargets(entries) {
  const fullyIntersecting = entries.filter(
    (entry) => entry.isIntersecting && entry.intersectionRatio === 1
  )[0];
  if (fullyIntersecting) {
    if (selectedVideo) {
      selectedVideo.load();
    }
    selectedVideo = fullyIntersecting.target;
    selectedVideo.play();
  }
}

export default new IntersectionObserver(filterTargets, {
  root: document.querySelector("#main"),
  rootMargin: "0px",
  threshold: [0, 1.0],
});
