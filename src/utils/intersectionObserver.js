import videoSlice from "store/video";
import store from "store";

function filterTargets(entries) {
  const fullyIntersecting = entries.filter(
    (entry) => entry.isIntersecting && entry.intersectionRatio === 1
  )[0];
  if (fullyIntersecting) {
    store.dispatch(
      videoSlice.actions.setCurrentVideo(fullyIntersecting.target.id)
    );
  }
}

export default new IntersectionObserver(filterTargets, {
  root: document.querySelector("#main"),
  rootMargin: "0px",
  threshold: 1.0,
});
