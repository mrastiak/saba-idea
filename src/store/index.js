import { configureStore } from "@reduxjs/toolkit";
import videoSlice from "store/video";

export default configureStore({
  reducer: videoSlice.reducer,
});
