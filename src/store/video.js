import { createSlice } from "@reduxjs/toolkit";

const videoSlice = createSlice({
  name: "video",
  initialState: 0,
  reducers: {
    setCurrentVideo: (state, { payload }) => payload,
  },
});

export default videoSlice;
